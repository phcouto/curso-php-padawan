<?php
require 'Quadrado.php';
require 'Circulo.php';
require 'Trapezio.php';
require 'CalculadoraFiguras.php';
require 'FiguraException.php';
$quadrado = new Quadrado();
$quadrado->setLado(10);

$c = new Circulo();
try{
    $c->setRaio(-1);
}catch(FiguraException $e) {
    echo $e->getMenssagem();
}

$calculadora = new CalculadoraFiguras();

$calculadora->setFigura($quadrado);

echo "Area = {$calculadora->calculoArea()}\n";

$calculadora->setFigura($c);

echo "Area = {$calculadora->calculoArea()}\n\n";


$t = new Trapezio();

$calculadora = new CalculadoraFiguras();

$calculadora->setFigura($t);
echo "Area = {$calculadora->calculoArea()}\n\n";