<?php
require 'Figura.php';
class Quadrado implements Figura {

    private $lado;

    public function calcularArea() {
        return $this->lado * $this->lado;
    }


    /**
     * Gets the value of lado.
     *
     * @return mixed
     */
    public function getLado()
    {
        return $this->lado;
    }

    /**
     * Sets the value of lado.
     *
     * @param mixed $lado the lado
     *
     * @return self
     */
    public function setLado($lado)
    {
        $this->lado = $lado;

        return $this;
    }
}