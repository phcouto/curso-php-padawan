<?php
require_once 'Pessoa.php';
require_once 'Calculo.php';

    class Masculino extends Pessoa implements Calculo{
        public function __construct($nome, $peso, $altura)
        {
            $sexo = 'Masculino';
            parent::__construct($nome, $peso, $sexo, $altura);
        }

        public function Calcular() {
            $r = $this->peso / ($this->altura * $this ->altura);
            if ($r < 20) {
                return 'Abaixo do Peso';
            }
            else if($r >= 20 && $r < 25){
                return 'peso ideal';
            }
            else {
                return 'acima do peso';
            }
        }



    }

?>