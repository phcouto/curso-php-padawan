<?php

$nome = 'Curso PHP Padawan'; // String e somente string
$curso = "O nome do curso dos devs abril é $nome"; // String "dinâmica"

$nomeDoCurso = $nome; // convencao para nomes de variaveis compostas

$inteiro = 1;
$numeroReal = 1.2;
$octal = 0123;
$hexa = 0x1A;
$nulo = null;
$binario = 0b1111;

$vetor = array("Curso", "PHP", "Padawan");
$objeto = new StdClass();