### Criar um programa em PHP que acesse a API editorial do Alexandria e:

* Liste as matérias
* Monte um template html bonitinho para cada uma das matérias listadas

#### Dados importantes

URL da api:

```
http://editorial.api.alexandria.com.br
```

Alguns parâmetros:

* ``?pw=2`` -> Página 2
* ``marca=viajeaqui`` -> Filtra pela marca

#### Requisitos:

* Orientado a objetos
* Levar em conta as boas práticas

Vocês são os eng. de software. A solução é de vocês. :D
