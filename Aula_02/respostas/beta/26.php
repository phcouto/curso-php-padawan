<?php

/*
A partir de três valores que serão digitados, verificar se formam ou não um triângulo. Em caso positivo, exibir sua classificação: “Isósceles, escaleno ou eqüilátero”. Um triângulo escaleno possui todos os lados diferentes, o isósceles, dois lados iguais e o eqüilátero, todos os lados iguais. Para existir triângulo é necessário que a soma de dois lados quaisquer seja maior que o outro, isto, para os três lados.
 
Mulheres

R < 19       | Abaixo do peso
19 <= R < 24 | Peso ideal
R >= 24      | Acima do peso

Homens

R < 20       | Abaixo do peso
20 <= R < 25 | Peso ideal
R >= 25      | Acima do peso


 */