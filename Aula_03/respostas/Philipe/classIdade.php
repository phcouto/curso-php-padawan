<?php

class Idade
{
    function __construct($dia, $mes, $ano)
    {
        $this->dia = $dia;
        $this->mes = $mes;
        $this->ano = $ano;
    }

    function validar_dados($dia, $mes, $ano)
    {
        if (empty($dia) || empty($mes) || empty($ano))
        throw new InvalidArgumentException("Valores em branco", 1);

        if (!is_numeric($dia) || !is_numeric($mes) || !is_numeric($ano))
        throw new InvalidArgumentException("Data, mês e ano deve ser inteiro positivo", 1);

        return true;
    }

    function calcular_idade($dia, $mes, $ano)
    {
        $hoje = getdate();

        $idade = $hoje['year'] - $ano;
        if ($hoje['mon'] < $mes || ($hoje['mon'] == $mes && $hoje['mday'] < $dia))
        {
            $idade -= 1;
        }

        return $idade;
    }
}

 ?>
