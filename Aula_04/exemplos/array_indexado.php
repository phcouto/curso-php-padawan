<?php

	$vetor[0] = 'Leonam';
	$vetor[1] = 'Bruno';
	$vetor[2] = 'Beta';
	$vetor[3] = 'Wendel';
	$vetor[4] = 'Vinicius';
	$vetor[5] = 'Philipe';

	$nomes = array('Leonam','Bruno','Curso','PHP','Padawan');

	// iterando sobre o array e printando cada item utilizando o for
	for ($contador=0; $contador < count($vetor) -1; $contador++) { 
		echo $vetor[$contador]."\n";
	}

	// iterando sobre o array e printando cada item utilizando o foreach
	foreach ($vetor as $chave => $nome) {
		echo "O nome $nome está no índice $chave\n";
	}

?>